# PdoQuery

A general SQL query object that uses prepared statements.

## Badges
![Image](https://yuml.me/41ff4f2b.svg)
![img2](https://yuml.me/diagram/plain/class/%5BDs%C2%ABCollection%C2%BB%7CgetEntries%28%29%3BisEmpty%28%29%7Bbg%3Aorchid%7D%5D%2C%5BDs%C2%ABSequence%C2%BB%3B_ordered_%7CgetNext%28%29%3Badd%28%C2%A7%29%3Bsub%28%C2%A7%29%7Bbg%3Aorchid%7D%5D%2C%5BDs%C2%ABSequence%C2%BB%5D-.-%5E%5BDs%C2%ABCollection%C2%BB%5D%2C%5BDsIndexedList%7C%C2%A7entries%7Cadd%28%C2%A7%29%3Bremove%28%C2%A7%29%3Binsert%28%C2%A7%29%7Bbg%3Aviolet%7D%5D-.-%5E%5BDs%C2%ABSequence%C2%BB%5D%2C%5BDsSetList%3B_ordered+unique_%7C%7Bbg%3Aviolet%7D%5D-%5E%5BDsIndexedList%5D%2C%5BDsIntegerSet%7C%7Bbg%3Aviolet%7D%5D-%5E%5BDsSetList%5D%2C%5BModelValueObject%7CsetValue%28%29%7Bbg%3Askyblue%7D%5D%2C%5BModelIntegerValue%7C%C2%A7value%7CgetValue%28%29%3BsetValue%28%29%7Bbg%3Askyblue%7D%5D-%5E%5BModelValueObject%5D%2C%5BPitchClass%7C%C2%A7name%3B%C2%A7accidental%7Bbg%3Apaleturquoise%7D%5D-%5E%5BModelIntegerValue%5D%2C%5BOctave%7CgetPitches%28%29%7Bbg%3Apaleturquoise%7D%5D-%5E%5BModelIntegerValue%5D%2C%5BPitch%7C%C2%A7frequency%3B%C2%A7pitchClass%3B%C2%A7accidental%3B%C2%A7octave%7CgetPitchClass%28%29%3BgetAccidental%28%29%3BgetOctave%28%29%3BgetFrequency%28%29%7Bbg%3Apaleturquoise%7D%5D-%5E%5BModelIntegerValue%5D%2C%5BAccidental%7C%C2%A7symbol%3B%C2%A7name%7Bbg%3Apaleturquoise%7D%5D-%5E%5BModelIntegerValue%5D%2C%5BIntervalList%7C%7Bbg%3Aplum%7D%5D-%5E%5BDsIndexedList%5D%2C%5BIntervalSetList%7C%7Bbg%3Aplum%7D%5D-%5E%5BDsSetList%5D%2C%5BPattern%7C%C2%A7intervals%3B%C2%A7isSetList%7CasList%28%29%3BasSet%28%29%7Bbg%3Aplum%7D%5D-%5E%5BIntervalList%5D%2C%5BFormula%7C%7Bbg%3Aplum%7D%5D-%5E%5BDsSetList%5D%2C%5BPitchClassSet%7C%7Bbg%3Aplum%7D%5D-%5E%5BDsIntegerSet%5D%2C%5BChordFactors%7C%7Bbg%3Aplum%7D%5D-%5E%5BPitchClassSet%5D%2C%5BPitchList%7C%7Bbg%3Aplum%7D%5D-%5E%5BDsIndexedList%5D%2C%5BChordVoicing%7C%7Bbg%3Aplum%7D%5D-%5E%5BPitchList%5D%2C%5BChord%7C%7Bbg%3Aplum%7D%5D%2C%5BChord%5D-.-%3E%5BChordFactors%5D%2C%5BChord%5D-.-%3E%5BChordVoicing%5D%2C%5B%2AMap%2A%7Bbg%3Acyan%7D%3B_key+value_%5D%2C%5B%2ASet%2A%7Bbg%3Acyan%7D%3B_key%3Dvalue_%5D%2C%5B%2ASequence%2A%7Bbg%3Acyan%7D%3B_ordered+keys_%3B0.1.2...X%5D%2C%5B%2AIndex%2A%7Bbg%3Acyan%7D%3B_int-key+map_%5D--%5E%5B%2AMap%2A%5D%2C%5B%2ADictionary%2A%7Bbg%3Acyan%7D%3B_string-key+map_%5D--%5E%5B%2AMap%2A%5D%2C%5B%2ASetList%2A%7Bbg%3Acyan%7D%5D--%5E%5B%2ASet%2A%5D%2C%5B%2ASet%2A%5D--%5E%5B%2AMap%2A%5D%2C%5B%2ASequence%2A%5D--%5E%5B%2AIndex%2A%5D%2C%5B%2ASetList%2A%5D--%5E%5B%2ASequence%2A%5D.svg)

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
issue tracker, a chat room, an email address, etc.

## Roadmap
ideas for releases in the future.

## Contributing
open to contributions.

## Authors and acknowledgment
who has contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
Brainstorming