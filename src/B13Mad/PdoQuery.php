<?php

/**
 * B13 PHP Library depends on the Infra Standard. {@see https://infra.spec.whatwg.org}
 * 
 * @author CIID Mike Belfie <belfie13@hotmail.com>
 * @copyright 2008-2024 B13 Australia. All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL-3.0-only
 */

declare(strict_types=1);

namespace B13Mad;

use PDO;


/**
 * 
 */
class PdoQuery
 {
    public function __construct(protected PDO $pdo)
     {
     }
    /**
     * Adds a new entry to the database for the given $table and $data.
     */
    public function insert(string $table, array $data)
     {
        $sql  = "INSERT INTO $table (";
        $sql .= implode(', ', array_keys($data)) . ') VALUES (';
        $sql .= rtrim(str_repeat('?, ', count($data)), ', ') . ')';

        $statement = $this->pdo->prepare($sql);
        $statement->execute(array_values($data));
        return $this->pdo->lastInsertId();
     }
    /**
     * @param array $parameters associative array of name => value pairs
     */
    public function select(string $table, array $parameters)
     {
        $sql  = "SELECT * FROM $table WHERE ";
        $sql .= implode(' = ? AND ', array_keys($parameters)) . ' = ?';

        $statement = $this->pdo->prepare($sql);
        $statement->execute(array_values($parameters));
        return $statement->fetchAll();
     }
    /**
     * Update an entry with $data referenced by $id in the given $table.
     */
    public function update(string $table, array $data, string $id)
     {
        $sql  = "UPDATE $table SET ";
        $sql .= implode(' = ?, ', array_keys($data)) . ' = ? WHERE id = ?';

        // try
        //  {
            $statement = $this->pdo->prepare($sql);
        //  }
        // catch (PDOException $exception)
        //  {
        //     echo 'Error! '.$exception->getMessage();
        //  }
        
        $values = array_merge(array_values($data), [$id]);
        $statement->execute($values);
        return $statement->rowCount();
     }
    /**
     * 
     */
    public function delete(string $table, string $id)
     {
        $sql = "DELETE FROM $table WHERE id = ?";

        $statement = $this->pdo->prepare($sql);
        $statement->execute([$id]);
        return $statement->rowCount();
     }
 }
